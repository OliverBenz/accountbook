package at.ran.ws.db;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBHelper {

	public static Connection getConnection() throws SQLException {
		// setup the connection with the DB.
		Connection connect;
		try {
			// this will load the MySQL driver, each DB has its own driver
			String url = "jdbc:mysql://localhost/db_cars?user=root&password=";

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url);
			return conn;
		} catch (ClassNotFoundException e) {
			return null;
		}

	}

}