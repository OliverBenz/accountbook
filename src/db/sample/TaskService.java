package at.ran.ws.sample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import at.ran.ws.db.dao.carDAO;

@Path("/cars")
public class TaskService {

	@GET
	@Path("/all")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<car> getAllCars() {
		List<car> cars = carDAO.getAllCars();

		return cars;

	}

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public car getCarForId(@PathParam("id") int id) {
		
		return carDAO.getCar(id);
		

	}

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public carInsert addCar(carInsert car) {
		carDAO.insertCars(car);
		return car;

	}

}
