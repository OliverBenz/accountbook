package at.ran.ws.sample;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class account {
	private int id;
	private String website;
	private String user;
	private String password;
	private String info;
	private int date;
	private int likes;
	private int dislikes;

	
	
	
	public account() {
		super();
		// TODO Auto-generated constructor stub
	}


	public int getDate() {
		return date;
	}


	public void setDate(int date) {
		this.date = date;
	}


	public int getLikes() {
		return likes;
	}


	public void setLikes(int likes) {
		this.likes = likes;
	}


	public int getDislikes() {
		return dislikes;
	}


	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}


	public String getWebsite() {
		return website;
	}


	public void setWebsite(String website) {
		this.website = website;
	}

	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getBrand() {
		return user;
	}


	public void setBrand(String brand) {
		this.user = brand;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public account(int id, String website, String user, String password, int date, int likes, int dislikes) {
		super();
		this.id = id;
		this.website = website;
		this.user = user;
		this.password = password;
		this.date = date;
		this.likes = likes;
		this.dislikes = dislikes;


	}
}
