package at.ran.ws.sample;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class carInsert {
	private int id;
	private String brand;
	private String model;
	private int year;
	private int kilometers;
	private int hp;
	private String fuel;
	private int price;
	
	
	
	public carInsert() {
		super();
		// TODO Auto-generated constructor stub
	}


	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public int getKilometers() {
		return kilometers;
	}


	public void setKilometers(int kilometers) {
		this.kilometers = kilometers;
	}


	public int getHp() {
		return hp;
	}


	public void setHp(int hp) {
		this.hp = hp;
	}


	public String getFuel() {
		return fuel;
	}


	public void setFuel(String fuel) {
		this.fuel = fuel;
	}


	public int getPrice() {
		return price;
	}


	public void setPrice(int price) {
		this.price = price;
	}
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}


	public void setBrand(String brand) {
		this.brand = brand;
	}


	public String getModel() {
		return model;
	}


	public void setModel(String model) {
		this.model = model;
	}


	public carInsert(String brand, String model, int year, int kilometers, int hp, String fuel, int price) {
		super();
		this.brand = brand;
		this.model = model;
		this.year = year;
		this.kilometers = kilometers;
		this.hp = hp;
		this.fuel = fuel;
		this.price = price;
	}
}
