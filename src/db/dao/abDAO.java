package at.ran.ws.db.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import at.ran.ws.db.DBHelper;
import at.ran.ws.sample.car;
import at.ran.ws.sample.carInsert;

public class carDAO {
	
	public static void insertCars(carInsert cars) {

		try {

			Connection connection = DBHelper.getConnection();
			Statement statement = connection.createStatement();
			String sql = "INSERT INTO `tbl_cars`(`brand`, `model`, `year`, `kilometers`, `hp`, `fuel`, `price`) VALUES ('" + cars.getBrand() + "', '" + cars.getModel()+"', '" + cars.getYear()+ "', '" + cars.getKilometers()+ "', '" + cars.getHp()+ "', '" + cars.getFuel()+ "', '" + cars.getPrice()+ "')";
			System.out.println(sql);
			statement.execute(sql);
		

		
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static List<car> getAllCars() {
		List<car> cars = new ArrayList<car>();

		try {
			
			Connection connection = DBHelper.getConnection();
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from tbl_cars");
			rs.first();
			while (!rs.isAfterLast()) {
				cars.add(new car(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getInt(5),rs.getInt(6),rs.getString(7),rs.getInt(8)));
				

				rs.next();
			}

			rs.close();
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return cars;

	}
	
	public static car getCar(int ID) {
		car car = null;

		try {
			
			Connection connection = DBHelper.getConnection();
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from tbl_cars where ID = "+ ID);
			rs.first();
			
			car = new car(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getInt(5),rs.getInt(6),rs.getString(7),rs.getInt(8));

			rs.close();
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return car;

	}


}



	

